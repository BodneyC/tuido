import setuptools


with open('README.md') as f:
    desc = f.read()

setuptools.setup(
    name='tuido',
    version='0.0.3',
    scripts=['tuido'],
    author='BenJC',
    author_email='benjamin.carrington@gmail.com',
    description='TUI todo list',
    long_description=desc,
    long_description_content_type='text/markdown',
    url='https://github.com/bodneyc/tuido',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)'
    ]
)
